<#

Author: Carlos García García (@ciyinet)
Required Dependencies: PowerView (dev version)

#>


###########################################################
#################     General settings    #################
###########################################################
$PowerViewFile= "PowerView-new2.ps1"
###########################################################


# ASCII BANNER: http://www.genelaisne.com/powershell-warnings-with-ascii-art/

$banner = @"

8888888b.                                    8888888888                                    d888   
888  "Y88b                                   888                                          d8888   
888    888                                   888                                            888   
888    888  .d88b.  88888b.  888  888        8888888   888  888  .d88b.  888d888 888  888   888   
888    888 d8P  Y8b 888 "88b 888  888        888       888  888 d8P  Y8b 888P"   888  888   888   
888    888 88888888 888  888 888  888 888888 888       Y88  88P 88888888 888     888  888   888   
888  .d88P Y8b.     888  888 Y88b 888        888        Y8bd8P  Y8b.     888     Y88b 888   888   
8888888P"   "Y8888  888  888  "Y88888        8888888888  Y88P    "Y8888  888      "Y88888 8888888 
                                  888                                                 888         
                             Y8b d88P                                            Y8b d88P         
                              "Y88P"    ###     v0.1 - by Carlos García García (@ciyinet)     ###       
                                                                                                       

"@


$banner -split "`n" | % {
  Write-Host -Foreground Green  $_ 
}

Write-Host -Foreground Yellow 'Usage: Invoke-DenyEvery1 -Group "Group name"'

#Import PowerView dev version
try
{
    . "$PSScriptRoot\$PowerViewFile"
}
catch [System.Management.Automation.CommandNotFoundException]
{
    write-host "$PowerViewFile file not found. Please make sure the file is located at the same path." -ForegroundColor Red
    exit

}
catch 
{
    write-host "Caught an exception when trying to load PowerView:" -ForegroundColor Red
    write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
    write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
    exit
}


function Test-Administrator  
{  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
}



function Invoke-DenyEvery1 {

<#
.SYNOPSIS

Add Deny Everyone ACL to objects members of a group

Author: Carlos García García (@ciyinet)  
License: BSD 3-Clause  
Required Dependencies: PowerView Dev

.DESCRIPTION

This function adds an explicit deny Everyone ACL.


.PARAMETER Group

Specifies the group objects whose users will be added a deny Everyone ACL.


.EXAMPLE

Invoke-DenyEvery1 -Group "Administrators"

.LINK

http://

#>

    Param(
        [parameter(Mandatory=$true)]
        [String]
        $Group
    )

    if (-Not (Test-Administrator)) {
        Write-Host -ForegroundColor Red "Invoke-DenyEvery1 must run with admin privileges"
        return 
    } 

    #Receive the list of all members of the group
    $Members = Get-DomainGroupMember -Identity $Group -Recurse


    if ($Members -eq $null) {
        Write-Host -ForegroundColor Red "The group could not be found"
        return
    }

    foreach ($Item in $Members)
    {
        if ($Item.MemberObjectClass.Equals("user"))
        {
                
            # Deny 'Everyone' the property 'Read general information' for the user object
            $RawUser = Get-DomainUser -Raw -Identity $Item.MemberName
            $TargetUser = $RawUser.GetDirectoryEntry()

            $ACE = New-ADObjectAccessControlEntry -InheritanceType All -AccessControlType Deny -PrincipalIdentity "S-1-1-0" -Right ReadProperty -ObjectType '59ba2f42-79a2-11d0-9020-00c04fc2d3cf'
            $TargetUser.PsBase.ObjectSecurity.AddAccessRule($ACE)
            $TargetUser.PsBase.CommitChanges()
            write-host ("* OK * [USER]  Deny 'Everyone' the right to read general information of the object '{0}'" -f $Item.MemberName)
   
        }
        elseif ($Item.MemberObjectClass.Equals("group"))
        {
            # Get User's OU
            #$UserAux = Get-DomainUser quser02
            #$UserOU = $UserAux.distinguishedname.Substring($UserAux.distinguishedname.IndexOf("OU="))
            $RawObject = Get-DomainGroup -Raw -Identity $Item.GroupName
            $TargetObject = $RawObject.GetDirectoryEntry()
            # Deny 'Everyone' the property 'Read members' for the group object
            $ACE = New-ADObjectAccessControlEntry -InheritanceType All -AccessControlType Deny -PrincipalIdentity "S-1-1-0" -Right ReadProperty -ObjectType 'bf9679c0-0de6-11d0-a285-00aa003049e2'
            $TargetObject.PsBase.ObjectSecurity.AddAccessRule($ACE)
            $TargetObject.PsBase.CommitChanges()
            write-host ("* OK * [GROUP] Deny 'Everyone' the right to read members of the object '{0}'" -f $Item.MemberName)      
        }
    }

    #Deny the group itself (not only its member groups)
    $RawObject = Get-DomainGroup -Raw -Identity $Group
    $TargetObject = $RawObject.GetDirectoryEntry()
    # Deny 'Everyone' the property 'Read members' for the group object
    $ACE = New-ADObjectAccessControlEntry -Verbose -InheritanceType All -AccessControlType Deny -PrincipalIdentity "S-1-1-0" -Right ReadProperty -ObjectType 'bf9679c0-0de6-11d0-a285-00aa003049e2'
    $TargetObject.PsBase.ObjectSecurity.AddAccessRule($ACE)
    $TargetObject.PsBase.CommitChanges()
    write-host ("* OK * [GROUP] Deny 'Everyone' the right to read members of the object '{0}'" -f $Group) 

}


# Usually I get the ObjectType guid from Get-DomainGUIDMap,but that cmdlet needs to be changed(that GUID is an ExtendedRight and propertyGUID)
# Read General informatino = 59ba2f42-79a2-11d0-9020-00c04fc2d3cf